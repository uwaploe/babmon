# Deep Profiler Battery Monitor

Babmon periodically reads the Battery Aggregator Board (BAB) and sends the data records to the Data Archiver for storage. It also stores the most recent record in the Redis data store for use by other processes.
