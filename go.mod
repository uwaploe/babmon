module bitbucket.org/uwaploe/babmon

go 1.21.5

require (
	bitbucket.org/mfkenney/go-redisq/v3 v3.2.0
	bitbucket.org/uwaploe/bab v0.5.0
	bitbucket.org/uwaploe/dpipc v0.1.0
	github.com/gomodule/redigo v1.8.9
	github.com/tarm/serial v0.0.0-20180830185346-98f6abe2eb07
)

require (
	github.com/vmihailenco/msgpack/v5 v5.4.1 // indirect
	github.com/vmihailenco/tagparser/v2 v2.0.0 // indirect
	golang.org/x/sys v0.0.0-20190215142949-d0b11bdaac8a // indirect
)
