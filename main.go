// Babmon monitors the DPC Battery Aggregator Board
package main

import (
	"encoding/json"
	"flag"
	"fmt"
	"log"
	"os"
	"os/signal"
	"runtime"
	"strconv"
	"syscall"
	"time"

	redisq "bitbucket.org/mfkenney/go-redisq/v3"
	"bitbucket.org/uwaploe/bab"
	"bitbucket.org/uwaploe/dpipc"
	"github.com/gomodule/redigo/redis"
	"github.com/tarm/serial"
)

const Usage = `Usage: babmon [options] interval

Monitor the DPC Battery Aggregator Board at the specified interval. The interval
is written as a decimal value followed by a unit; 'm', 's', 'ms' (e.g. 10s)

`

const minInterval time.Duration = time.Second * 5

var Version = "dev"
var BuildDate = "unknown"

var (
	showVers = flag.Bool("version", false,
		"Show program version information and exit")
	doDebug   bool
	rdAddr    string        = "localhost:6379"
	dqName    string        = "archive:queue"
	qLen      uint          = 200
	rdKey     string        = "battery_status"
	babDev    string        = "/dev/ttyAT6"
	babBaud   int           = 4800
	timeout   time.Duration = time.Second * 4
	wattHours float64       = 120
	rdChan    string
)

func redisStore(conn redis.Conn, name string, t time.Time, summary bab.Summary) error {
	conn.Send("MULTI")
	conn.Send("HSET", name, "t", strconv.FormatInt(t.Unix(), 10))
	conn.Send("HSET", name, "count", strconv.Itoa(summary.Count))
	conn.Send("HSET", name, "temp", "0")
	conn.Send("HSET", name, "voltage", strconv.Itoa(summary.Voltage))
	conn.Send("HSET", name, "current", strconv.Itoa(summary.Amperage))
	energy := float64(summary.Count) * wattHours * float64(summary.Capacity) / 100
	conn.Send("HSET", name, "energy", strconv.Itoa(int(energy*1000)))
	conn.Send("HSET", name, "rel_charge", strconv.Itoa(summary.Capacity))
	conn.Send("HSET", name, "avg_current", strconv.Itoa(summary.Amperage))
	_, err := conn.Do("EXEC")

	return err
}

func redisPub(conn redis.Conn, name string, t time.Time, summary bab.Summary) error {
	b, err := json.Marshal(summary)
	if err != nil {
		return err
	}
	rec := struct {
		T    time.Time       `json:"t"`
		Data json.RawMessage `json:"data"`
	}{T: t, Data: json.RawMessage(b)}

	b, err = json.Marshal(rec)
	if err != nil {
		return err
	}

	_, err = conn.Do("PUBLISH", name, b)

	return err
}

func parseCmdLine() []string {
	flag.Usage = func() {
		fmt.Fprintf(os.Stderr, Usage)
		flag.PrintDefaults()
	}

	flag.StringVar(&rdAddr, "addr", rdAddr, "host:port for Redis server")
	flag.StringVar(&rdKey, "key", rdKey, "Redis key for battery status")
	flag.StringVar(&rdChan, "chan", rdChan, "Publish data on Redis channel")
	flag.StringVar(&dqName, "queue", dqName, "name of Redis list for the data queue")
	flag.UintVar(&qLen, "qlen", qLen, "maximum data queue length")
	flag.StringVar(&babDev, "dev", babDev, "serial device for BAB")
	flag.IntVar(&babBaud, "baud", babBaud, "BAB baud rate")
	flag.DurationVar(&timeout, "timeout", timeout, "BAB read timeout")
	flag.Float64Var(&wattHours, "wh", wattHours, "watt-hour capacity per battery")
	flag.Parse()

	if *showVers {
		fmt.Fprintf(os.Stderr, "%s %s\n", os.Args[0], Version)
		fmt.Fprintf(os.Stderr, "  Built with: %s\n", runtime.Version())
		os.Exit(0)
	}

	return flag.Args()
}

func dataRecord(t time.Time, rec bab.Summary) dpipc.DataRecord {
	return dpipc.DataRecord{
		Src: "battavg",
		T:   t,
		Data: map[string]interface{}{
			"count":    uint32(rec.Count),
			"capacity": uint32(rec.Capacity),
			"voltage":  float32(rec.Voltage) / 1000,
			"amperage": float32(rec.Amperage) / 1000,
		}}
}

func main() {
	args := parseCmdLine()

	interval, err := time.ParseDuration(args[0])
	if err != nil {
		log.Fatalf("Cannot parse interval: %q; %v", args[0], err)
	}

	if interval < minInterval {
		log.Fatalf("Interval must be >= %s", minInterval)
	}

	port, err := serial.OpenPort(&serial.Config{
		Name:        babDev,
		Baud:        babBaud,
		ReadTimeout: timeout,
	})
	if err != nil {
		log.Fatalf("Cannot open serial device %q: %v", babDev, err)
	}
	defer port.Close()

	conn, err := redis.Dial("tcp", rdAddr)
	if err != nil {
		log.Fatalf("Cannot connect to Redis server: %v", err)
	}
	defer conn.Close()

	dataq := redisq.NewQueue[dpipc.DataRecord](conn, dqName, qLen)

	// Initialize signal handler
	sigs := make(chan os.Signal, 1)
	signal.Notify(sigs, syscall.SIGINT, syscall.SIGTERM, syscall.SIGHUP)
	defer signal.Stop(sigs)

	log.Printf("DPC battery monitor (%s)", Version)
	dev := bab.New(port)
	ticker := time.NewTicker(interval)
	for {
		select {
		case t := <-ticker.C:
			summary, err := dev.GetAve()
			if err != nil {
				log.Fatalf("BAB GetAve() failed: %v", err)
			}
			summary.Amperage = summary.Amperage * summary.Count
			rec := dataRecord(t, summary)
			if err = dataq.Put(rec); err != nil {
				log.Printf("Cannot archive record: %v", err)
			}
			redisStore(conn, rdKey, t, summary)
			if rdChan != "" {
				redisPub(conn, rdChan, t, summary)
			}
		case <-sigs:
			return
		}
	}
}
